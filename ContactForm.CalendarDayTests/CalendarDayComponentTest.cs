﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace CalendarDayTests
{
    [TestClass]
    public class CalendarDayComponentTest
    {
        [TestMethod]
        [DataRow("2000-01-01", "2000-01-12", 10, "2000-01-05,2000-01-06")]
        [DataRow("2000-03-01", "2000-03-06", 6, "")]
        [DataRow("2000-01-01", "2000-01-08", 6, "2000-01-07")]
        [DataRow("2000-01-01", "2000-01-10", 6, "2000-01-06,2000-01-07,2000-01-08,2000-01-09")]
        [DataRow("2000-12-31", "2001-01-10", 10, "2000-01-01,2001-01-05")]
        [DataRow("2000-12-31", "2001-01-12", 10, "2000-01-01,2000-12-31,2001-01-05,2001-01-11")]
        [DataRow("2001-06-30", "2001-07-25", 24, "2001-07-02,2001-07-06")]
        [DataRow("2003-12-12", "2003-12-24", 12, "2003-12-15")]
        [DataRow("2008-08-06", "2008-09-16", 42, "")]
        [DataRow("2001-04-28", "2001-05-08", 8, "2001-04-28,2001-04-29,2001-04-30")]
        [DataRow("2000-03-01", "2000-03-01", 0, "")]
        [DataRow("2000-01-01", "2000-01-03", 2, "2000-01-01")]
        public void CalendarDayComponent_OnGetNextDay_ReturnsNextWorkDayAccordingToWeekensAndHolidays(string startday,
            string expectedDay, int offset, string daysOff)
        {
            var mock = new Mock<ICalendarDayRepository>();
            var cdc = new CalendarDayComponent(mock.Object);
            var dayValue = ParseExactDate(startday);
            var expectedDayValue = ParseExactDate(expectedDay);
            var daysOffList = ParseDaysList(daysOff);

            var years = daysOffList.Select(day => day.Day.Year).Distinct();

            foreach (var year in years)
            {
                mock.Setup(component => component.GetYearNotWorkingCalendarDays(year))
                    .Returns(daysOffList.Where(day => day.Day.Year == year));
            }

            var nextDay = cdc.GetNextDay(dayValue, offset);
            Assert.AreEqual(expectedDayValue, nextDay, $"{nextDay} != {expectedDayValue}");
        }

        private static IEnumerable<CalendarDay> ParseDaysList(string daysOff)
        {
            return daysOff.Split(',').Where(item => !string.IsNullOrEmpty(item))
                .Select(dayString => new CalendarDay
                {
                    Day = ParseExactDate(dayString),
                    IsHoliday = true
                });
        }

        private static DateTime ParseExactDate(string startday)
        {
            return DateTime.ParseExact(startday, "yyyy-MM-dd", CultureInfo.InvariantCulture);
        }
    }
}