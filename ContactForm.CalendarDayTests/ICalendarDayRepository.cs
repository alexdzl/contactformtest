﻿using System.Collections.Generic;

namespace CalendarDayTests
{
    public interface ICalendarDayRepository
    {
        IEnumerable<CalendarDay> GetYearNotWorkingCalendarDays(int year);
    }
}