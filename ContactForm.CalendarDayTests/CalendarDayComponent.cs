﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CalendarDayTests
{
    public class CalendarDayComponent
    {
        private readonly ICalendarDayRepository _calendarDayRepository;
        public bool IncludeFistDay { set; get; } = true;
        public CalendarDayComponent(ICalendarDayRepository calendarDayRepository)
        {
            _calendarDayRepository =
                calendarDayRepository ?? throw new ArgumentNullException(nameof(calendarDayRepository));
        }

        public DateTime GetNextDay(DateTime d, int dayMove)
        {
            if (dayMove > 0)
            {
                var nextday = d.AddDays(dayMove + GetHolidaysCountBetweenDates(d, d.AddDays(dayMove)) -
                                        (IncludeFistDay ? 1 : 0));
                return GetNextWorkDay(d, nextday);
            }

            return GetNextWorkDay(d, d);
        }

        private int GetHolidaysCountBetweenDates(DateTime start, DateTime end)
        {
            return GetYearNotWorkingCalendarDaysForYears(start.Year, end.Year)
                .Count(date => date.Day >= start && date.Day <= end);
        }

        private IEnumerable<CalendarDay> GetYearNotWorkingCalendarDaysForYears(int yearStart, int yearEnd)
        {
            var yearNotWorkingCalendarDays = _calendarDayRepository.GetYearNotWorkingCalendarDays(yearStart);
            while (yearStart != yearEnd)
            {
                ++yearStart;
                yearNotWorkingCalendarDays = yearNotWorkingCalendarDays
                    .Concat(_calendarDayRepository.GetYearNotWorkingCalendarDays(yearStart));
            }

            return yearNotWorkingCalendarDays;
        }

        private DateTime GetNextWorkDay(DateTime start, DateTime end)
        {
            var yearNotWorkingCalendarDays = GetYearNotWorkingCalendarDaysForYears(start.Year, end.Year);
            while (yearNotWorkingCalendarDays.Any(holiday => holiday.Day == end))
            {
                end = end.AddDays(1);
            }

            return end;
        }
    }
}