﻿using System;

namespace ContactForm.App.Exceptions
{
    public class ApplicationException : Exception
    {
        public ApplicationException(string message)
            : base(message) { }
    }
}