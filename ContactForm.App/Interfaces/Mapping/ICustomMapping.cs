﻿using AutoMapper;

namespace ContactForm.App.Interfaces.Mapping
{
    public interface ICustomMapping
    {
        void CreateMappings(Profile configuration);
    }
}
