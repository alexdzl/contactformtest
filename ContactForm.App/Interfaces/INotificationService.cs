﻿using System.Threading.Tasks;
using ContactForm.App.Notifications;

namespace ContactForm.App.Interfaces
{
    public interface INotificationService
    {
        Task SendAsync(Message message);
    }
}
