﻿using System.Threading;
using System.Threading.Tasks;
using ContactForm.Domain;

namespace ContactForm.App.Interfaces
{
    public interface IUnitOfWork
    {
        IAsyncRepository<Contact> ContactRepository { get; }
        Task<int> Commit(CancellationToken cancellationToken);
    }
}
