﻿using System;
using System.Threading;
using System.Threading.Tasks;
using ContactForm.App.Exceptions;
using ContactForm.App.Interfaces;
using ContactForm.Domain;
using MediatR;

namespace ContactForm.App.Contacts.DeleteContractCommand
{
    public class DeleteContractCommand : IRequest
    {
        public string Id { get; set; }

        public class Handler : IRequestHandler<DeleteContractCommand, Unit>
        {
            private IUnitOfWork _unitOfWork;

            public Handler(IUnitOfWork unitOfWork)
            {
                this._unitOfWork = unitOfWork;
            }

            public async Task<Unit> Handle(DeleteContractCommand request, CancellationToken cancellationToken)
            {
                if (Guid.TryParse(request.Id, out var id))
                {
                    var entity = await _unitOfWork.ContactRepository.GetById(id);
                    if (entity == null)
                    {
                        throw new NotFoundException(nameof(Contact), request.Id);
                    }

                    _unitOfWork.ContactRepository.Remove(entity);

                    await _unitOfWork.Commit(cancellationToken);

                    return Unit.Value;
                }
                throw new IdIncorrectException(nameof(Contact), request.Id);
            }
        }
    }
}