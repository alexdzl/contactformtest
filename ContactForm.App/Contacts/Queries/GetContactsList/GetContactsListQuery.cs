﻿using MediatR;

namespace ContactForm.App.Contacts.Queries.GetContactsList
{
    public class GetContactsListQuery : IRequest<ContactsListViewModel>
    {
    }
}