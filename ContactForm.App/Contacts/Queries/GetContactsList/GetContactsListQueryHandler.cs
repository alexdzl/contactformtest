﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using ContactForm.App.Interfaces;
using MediatR;

namespace ContactForm.App.Contacts.Queries.GetContactsList
{
    public class GetContactsListQueryHandler : IRequestHandler<GetContactsListQuery, ContactsListViewModel>
    {
        private IUnitOfWork _unitOfWork;
        private IMapper _mapper;
        public GetContactsListQueryHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            this._unitOfWork = unitOfWork;
            this._mapper = mapper;
        }

        public async Task<ContactsListViewModel> Handle(GetContactsListQuery getContactsListQuery, CancellationToken token)
        {
            return new ContactsListViewModel()
            {
                Contacts = await GetViewModels()
            };
        }

        private async Task<List<ContactLookupModel>> GetViewModels()
        {
            var entities = await _unitOfWork.ContactRepository.Get();
            return _mapper.Map<List<ContactLookupModel>>(entities);
        }
    }
}
