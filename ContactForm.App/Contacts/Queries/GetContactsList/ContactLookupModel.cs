﻿using System;

namespace ContactForm.App.Contacts.Queries.GetContactsList
{
    public class ContactLookupModel
    {
        public Guid Id { get; set; }
        public string Email { get; set; }
        public string Comment { get; set; }
    }
}