﻿using System.Collections.Generic;

namespace ContactForm.App.Contacts.Queries.GetContactsList
{
    public class ContactsListViewModel
    {
        public IList<ContactLookupModel> Contacts { get; set; }
    }
}