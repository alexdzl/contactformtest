﻿using System;
using System.Threading;
using System.Threading.Tasks;
using ContactForm.App.Interfaces;
using ContactForm.App.Notifications;
using MediatR;

namespace ContactForm.App.Contacts.CreateContactCommand
{
    public class ContractCreated : INotification
    {
        public Guid Id { get; set; }
        public string Email { get; set; }
        public class ContactCreatedHandler : INotificationHandler<ContractCreated>
        {
            private readonly INotificationService _notification;

            public ContactCreatedHandler(INotificationService notification)
            {
                _notification = notification;
            }

            public async Task Handle(ContractCreated notification, CancellationToken cancellationToken)
            {
                await _notification.SendAsync(new Message()
                {
                    Subject = "ContractCreated",
                    Body = notification.Id.ToString()
                });
            }
        }

        public class ContactCreatedQueueHandler : INotificationHandler<ContractCreated>
        {
            private readonly INotificationService _notification;

            public ContactCreatedQueueHandler(INotificationService notification)
            {
                _notification = notification;
            }

            public async Task Handle(ContractCreated notification, CancellationToken cancellationToken)
            {
                //Add contact id to lead queue
                return;
            }
        }
    }
}