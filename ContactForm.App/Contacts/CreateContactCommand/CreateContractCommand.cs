﻿using System.Threading;
using System.Threading.Tasks;
using ContactForm.App.Interfaces;
using ContactForm.Common;
using ContactForm.Domain;
using MediatR;

namespace ContactForm.App.Contacts.CreateContactCommand
{
    public class CreateContractCommand : IRequest
    {
        public string Email { get; set; }
        public string Comment { get; set; }
        public AddressType Address { get; set; }
        public string[] Tags { get; set; }
        public bool SendNews { get; set; }
        public short? Gender { get; set; }
        public bool Accept { get; set; }
        public class AddressType
        {
            public string Country { get; set; }
            public string City { get; set; }
        }
        public class Handler : IRequestHandler<CreateContractCommand, Unit>
        {
            private readonly IUnitOfWork _unitOfWork;
            private readonly IMediator _mediator;
            private readonly IDateTime _sourceDateTime;
            
            public Handler(IUnitOfWork unitOfWork, IMediator mediator, IDateTime sourceDateTime)
            {
                _unitOfWork = unitOfWork;
                _mediator = mediator;
                _sourceDateTime = sourceDateTime;
            }

            public async Task<Unit> Handle(CreateContractCommand request, CancellationToken cancellationToken)
            {
                Gender? gender = null;
                if (request.Gender.HasValue)
                {
                    gender = request.Gender == 0 ? Domain.Gender.Female : Domain.Gender.Male;
                }

                var entity = new Contact(request.Email, request.Comment, request.Tags, request.Address.City, request.Address.Country, request.SendNews, gender);

                entity.CreatedDate = _sourceDateTime.UtcNow;

                await _unitOfWork.ContactRepository.Add(entity);

                await _unitOfWork.Commit(cancellationToken);

                await _mediator.Publish(new ContractCreated { Id = entity.Id, Email = entity.Email },
                    cancellationToken);

                return Unit.Value;
            }
        }
    }
}
