﻿using System.Linq;
using FluentValidation;
using FluentValidation.Validators;

namespace ContactForm.App.Contacts.CreateContactCommand
{
    public class CreateContactCommandValidator : AbstractValidator<CreateContractCommand>
    {
        public CreateContactCommandValidator()
        {
            RuleFor(contact => contact.Email)
                .Cascade(CascadeMode.StopOnFirstFailure)
                .NotEmpty().WithMessage("Email is required.")
                .EmailAddress().WithMessage("Invalid email format.")
                .Custom(RestrictedEmails);
            RuleFor(x => x.Address).NotEmpty();
            RuleFor(x => x.Address).Must(NotEmpty);
            RuleFor(contact => contact.Comment).NotEmpty().MinimumLength(6);
            RuleFor(x => x.Accept).Equal(true);

        }

        private bool NotEmpty(CreateContractCommand.AddressType address)
        {
            return !string.IsNullOrEmpty(address.Country) && !string.IsNullOrEmpty(address.City);
        }

        private void RestrictedEmails(string email, CustomContext context)
        {
            var restrictedEmailsFromService = new[] {"restr@email.com"};
            if (restrictedEmailsFromService.Contains(email))
            {

                context.AddFailure($"{email} is restricted");
            }
        }


    }
}