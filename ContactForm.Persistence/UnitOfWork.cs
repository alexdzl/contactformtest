﻿using System;
using System.Threading;
using System.Threading.Tasks;
using ContactForm.App.Interfaces;
using ContactForm.Domain;
using ContactForm.Persistence.Repositories;
using Microsoft.EntityFrameworkCore;

namespace ContactForm.Persistence
{
    public class UnitOfWork : IUnitOfWork
    {
        private IAsyncRepository<Contact> _contactRepository;
        private readonly DbContext _databaseContext;
        public UnitOfWork(DbContext context)
        {
            _databaseContext = context;
        }

        public IAsyncRepository<Contact> ContactRepository => 
            _contactRepository ?? (_contactRepository = new EfGenericAsyncRepository<Contact>(_databaseContext));

        public async Task<int> Commit(CancellationToken cancellationToken)
        {
            int save = await _databaseContext.SaveChangesAsync(cancellationToken);
            return await Task.FromResult(save);
        }
    }
}