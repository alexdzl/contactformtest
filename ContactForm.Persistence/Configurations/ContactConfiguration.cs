﻿using System;
using ContactForm.Domain;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ContactForm.Persistence.Configurations
{
    public class ContactConfiguration : IEntityTypeConfiguration<Contact>
    {
        public void Configure(EntityTypeBuilder<Contact> builder)
        {
            builder.HasKey(e => e.Id);
            builder.Property(e => e.Id).HasColumnName("ContactID");
            builder.Property(e => e.CreatedDate).HasColumnType("datetime");
            builder.Property(e => e.City)
                .HasMaxLength(15)
                .IsRequired();
            builder.Property(e => e.Country)
                .HasMaxLength(15)
                .IsRequired();
            builder.Property(e => e.Comment).HasMaxLength(255);
            builder.Property(e => e.Email).HasMaxLength(255);
            builder.Property(e => e.Tags).HasConversion(
                v => string.Join(@"||", v),
                v => v.Split(new []{'|','|'}, StringSplitOptions.RemoveEmptyEntries));
        }
    }
}
