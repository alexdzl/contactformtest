﻿using System;
using System.Collections.Generic;
using System.Linq;
using ContactForm.Domain;

namespace ContactForm.Persistence
{
    public class DbInitializer
    {
        public static void Initialize(CtDbContext context)
        {
            var initializer = new DbInitializer();
            initializer.SeedAll(context);
        }

        private void SeedAll(CtDbContext context)
        {
            context.Database.EnsureCreated();

            if (context.Contacts.Any())
            {
                return; // Db has been seeded
            }

            SeedContacts(context);
        }

        private void SeedContacts(CtDbContext context)
        {
            var contacts = new List<Contact>();
            for (int i = 1; i <= 10; i++)
            {
                contacts.Add(new Contact(
                    $"email{i}@test.com",
                    $"Comment{i}",
                    new string[]{"Tag1", "Tag2"},
                    $"city{i}",
                    $"country{i}",
                    true));
                contacts[i-1].CreatedDate = DateTime.UtcNow;
            }


            context.Contacts.AddRange(contacts);
            context.SaveChanges();
        }
    }
}
