﻿using ContactForm.Domain;
using Microsoft.EntityFrameworkCore;

namespace ContactForm.Persistence
{
    public class CtDbContext : DbContext
    {
        public CtDbContext(DbContextOptions<CtDbContext> options): base(options)
        {
        }

        public DbSet<Contact> Contacts { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(CtDbContext).Assembly);
        }
    }
}
