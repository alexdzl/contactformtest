﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ContactForm.Persistence.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Contacts",
                columns: table => new
                {
                    ContactID = table.Column<Guid>(nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "datetime", nullable: false),
                    Email = table.Column<string>(maxLength: 255, nullable: true),
                    Comment = table.Column<string>(maxLength: 255, nullable: true),
                    City = table.Column<string>(maxLength: 15, nullable: false),
                    Country = table.Column<string>(maxLength: 15, nullable: false),
                    Tags = table.Column<string>(nullable: true),
                    Gender = table.Column<int>(nullable: true),
                    SendNews = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Contacts", x => x.ContactID);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Contacts");
        }
    }
}
