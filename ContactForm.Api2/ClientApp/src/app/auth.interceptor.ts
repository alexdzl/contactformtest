import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpHeaders } from "@angular/common/http";
import { Observable } from "rxjs";

export class AuthInterseptor implements HttpInterceptor{
    intercept(req: HttpRequest<any>, next: HttpHandler):
     Observable<HttpEvent<any>> {
        const cloned = req.clone({
            headers: req.headers.append('API_KEY', this.getApiKey())
        });
        return next.handle(cloned);
    }

    private getApiKey():string{
        return 'TOKEN';
    }
}