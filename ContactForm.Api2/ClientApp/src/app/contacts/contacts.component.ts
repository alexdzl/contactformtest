import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { FormValidators } from '../shared/validators/form.validators';
import { ContactsClient, ContactLookupModel, CreateContractCommand } from '../services/backend-api.generated.service';

@Component({
  selector: 'app-contacts',
  templateUrl: './contacts.component.pug',
  styleUrls: ['./contacts.component.css']
})
export class ContactsComponent implements OnInit {
  form: FormGroup;
  sendNews: false;
  cityMap = {
    ru:'Москва',
    ua:'Киев',
    by:'Минск'
  }
  error:'';
  contacts: ContactLookupModel[];
  loading:boolean;
  constructor(private ContactsClient:ContactsClient) { }

  ngOnInit() {
    this.initForm();
    this.UpdateList();
  }

  private UpdateList() {
    this.loading = true;
    this.ContactsClient.getAll()
      .subscribe(responce => {
        console.log('responce', responce);
        this.contacts = responce.contacts;
        this.loading = false;
      }, error => {
        this.error = error.message;
      });
  }

  private initForm() {
    //TODO add validation

    this.form = new FormGroup({
      email: new FormControl('', [
        Validators.email,
        Validators.required,
        FormValidators.restrictedEmails
      ], [
        FormValidators.uniqueEmail
      ]),
      comment: new FormControl(null, [
        Validators.required,
        Validators.minLength(6)
      ]),
      address: new FormGroup({
        country: new FormControl('ru', Validators.required),
        city: new FormControl('', Validators.required)
      }),
      tags: new FormArray([]),
      sendNews: new FormControl(false),
      gender:new FormControl(''),
      accept: new FormControl(false, [Validators.requiredTrue])
    });
  }

  submit(){

    if (this.form.invalid) {
      return; 
    } 

    var formValue = {...this.form.value};
    console.log('FormData', formValue);
    console.log('FormData', JSON.stringify(formValue));
    var tags = this.form.get('tags') as FormArray;
    
    let contact:CreateContractCommand = new CreateContractCommand();
    contact.init(formValue)
    this.ContactsClient
       .create(contact)
       .subscribe((response) => {
        tags.clear();
        this.form.reset();
        this.UpdateList();
    },error => {
      this.error = error.title;
      console.log(error);
    });
  }

  setCapital(){
    const cityKey = this.form.get('address').get('country').value;
    const city = this.cityMap[cityKey];

    this.form.patchValue({
      address:{city}
    });
  }

  addTag(){
    const control = new FormControl('', Validators.required);
    var tags = this.form.get('tags') as FormArray;
    tags.push(control);
  }

  delete(id:string) {
     this.ContactsClient
            .delete(id)
            .subscribe((response) => {
                this.UpdateList();
          });
  }
}
