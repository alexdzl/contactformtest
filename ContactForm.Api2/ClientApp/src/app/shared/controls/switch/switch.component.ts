import { Component, OnInit, Provider, forwardRef } from '@angular/core';
import { NG_VALUE_ACCESSOR, ControlValueAccessor } from '@angular/forms';

const VALUE_ACCESSOR: Provider = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(()=> SwitchComponent),
  multi:true
}

@Component({
  selector: 'app-switch',
  templateUrl: './switch.component.pug',
  styleUrls: ['./switch.component.css'],
  providers: [VALUE_ACCESSOR]
})
export class SwitchComponent implements ControlValueAccessor {

  private onChange = (value:any) => {}
  private onTouched = (value:any) => {}

  writeValue(obj: boolean): void {
    this.setState(obj);
  }
  registerOnChange(fn: any): void {
    this.onChange = fn;
  }
  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }
  setDisabledState?(isDisabled: boolean): void {

  }

  state = false

  setState(state:boolean) {
    this.state = state;
    this.onChange(this.state);
    this.onTouched(this.state);
  }
}
