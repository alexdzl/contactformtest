import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SwitchComponent } from './controls/switch/switch.component';
import { SpinnerComponent } from './controls/spinner/spinner.component';



@NgModule({
  declarations: [
    SwitchComponent,
    SpinnerComponent,
  ],
  imports: [
    CommonModule
  ],
  exports: [
    SwitchComponent,
    SpinnerComponent,
  ]
})
export class SharedModule { }
