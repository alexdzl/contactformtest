using System.Threading.Tasks;
using ContactForm.App.Contacts.CreateContactCommand;
using ContactForm.App.Contacts.DeleteContractCommand;
using ContactForm.App.Contacts.Queries.GetContactsList;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ContactForm.Api2.Controllers
{
    public class ContactsController : BaseController
    {
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<ContactsListViewModel>> GetAll()
        {
            return Ok(await Mediator.Send(new GetContactsListQuery()));
        }

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesDefaultResponseType]
        public async Task<IActionResult> Create([FromBody]CreateContractCommand command)
        {
            await Mediator.Send(command);

            return NoContent();
        }

        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> Delete(string id)
        {
            await Mediator.Send(new DeleteContractCommand { Id = id });

            return NoContent();
        }
    }
}