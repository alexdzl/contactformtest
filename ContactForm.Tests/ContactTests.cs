using System;
using ContactForm.Domain;
using NUnit.Framework;

namespace ContactForm.Tests
{
    public class ContactTests
    {
        [SetUp]
        public void Setup()
        {
        }

   
        [Test]
        public void CreateContact__Should_Return_Contact()
        {
            //Arrange
            string expectEmail = "expect@email.com";
            string expectComment = "expect";

            //Act
            Contact actualContact = new Contact(expectEmail, expectComment,new []{"tag1"},"city","country", true);

            //Assert
            Assert.IsNotNull(actualContact);
            Assert.AreNotEqual(Guid.Empty, actualContact.Id);
            Assert.AreEqual(expectEmail, actualContact.Email);
            Assert.AreEqual(expectComment, actualContact.Comment);
        }
    }
}