﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ContactForm.App.Contacts.DeleteContractCommand;
using ContactForm.App.Exceptions;
using ContactForm.App.Interfaces;
using ContactForm.Domain;
using Moq;
using NUnit.Framework;

namespace ContactForm.Tests.Contacts.Commands.DeleteContact
{
    class DeleteContactCommandTests: CommandTestBase
    {
        private Contact _contact;
        private List<Contact> _contacts;

        [SetUp]
        public void Setup()
        {
            _contact = new Contact("11", "11", new[] {"tag1"}, "city1", "country1", true);
            _contacts = new List<Contact> {_contact};

            Mock<IAsyncRepository<Contact>> repositoryMock = new Mock<IAsyncRepository<Contact>>();
            _unitOfWork.Setup(uow => uow.ContactRepository).Returns(repositoryMock.Object);

            repositoryMock.Setup(r => r.GetById(It.IsAny<Guid>())).Returns<Guid>((id) =>
            {
                return Task.FromResult(_contacts.FirstOrDefault(contact => contact.Id == id)) ;
            });

            repositoryMock.Setup(r => r.Remove(It.IsAny<Contact>())).Callback<Contact>(contact =>
                {
                    _contacts.Remove(contact);
                });
        }

        [Test]
        public void Handle_ValidDeleteRequest_ShouldRemoveContact()
        {
            // Arrange
            var sut = new DeleteContractCommand.Handler(_unitOfWork.Object);
            string expectId = "8912e343-9769-4acd-a879-0ecae863ca40";
            _contact.Id = Guid.Parse(expectId);

            // Act
            var result = sut.Handle(new DeleteContractCommand
            {
                Id = expectId
            }, CancellationToken.None);

            // Assert
            Assert.That(_contacts, Has.Count.EqualTo(0));
        }

        [Test]
        public void Handle_InvalidDeleteRequest_ShouldThrowIdIncorrectException()
        {
            // Arrange
            var sut = new DeleteContractCommand.Handler(_unitOfWork.Object);
            string InvalidId = "";

            // Act
            // Assert
            Assert.ThrowsAsync<IdIncorrectException>(async () => await sut.Handle(new DeleteContractCommand
            {
                Id = InvalidId
            }, CancellationToken.None));
        }

        [Test]
        public void Handle_DeleteRequestWithNonExistingId_ShouldThrowNotFoundException()
        {
            // Arrange
            var sut = new DeleteContractCommand.Handler(_unitOfWork.Object);
            string nonExistingId = "1111e343-9769-4acd-a879-0ecae863ca40";
 
            // Act
            // Assert
            Assert.ThrowsAsync<NotFoundException>(async () => await sut.Handle(new DeleteContractCommand
            {
                Id = nonExistingId
            }, CancellationToken.None));
        }
    }
}
