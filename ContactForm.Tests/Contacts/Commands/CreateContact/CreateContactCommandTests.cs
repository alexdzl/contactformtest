using System;
using System.Threading;
using System.Threading.Tasks;
using ContactForm.App.Contacts.CreateContactCommand;
using ContactForm.App.Interfaces;
using ContactForm.Domain;
using ContactForm.Tests.Stubs;
using Moq;
using NUnit.Framework;

namespace ContactForm.Tests.Contacts.Commands.CreateContact
{
    public class CreateContactCommandTests : CommandTestBase
    {
        private Contact _contact;

        [SetUp]
        public void Setup()
        {
            _contact = null;

            Mock<IAsyncRepository<Contact>> repositoryMock = new Mock<IAsyncRepository<Contact>>();

            _unitOfWork.Setup(uow => uow.ContactRepository).Returns(repositoryMock.Object);

            repositoryMock.Setup(repo => repo.Add(It.IsAny<Contact>())).Returns<Contact>((contact) =>
            {
                _contact = contact;
                return Task.CompletedTask;
            });
        }

        [Test]
        public void Handle_GivenValidRequest_ShouldRaiseContactCreatedNotificationEvent()
        {
            DateTime expectTime = new DateTime(2000,1,1,0,0,0,DateTimeKind.Utc);
            // Arrange
            var sut = new CreateContractCommand.Handler(_unitOfWork.Object, _mediatorMock.Object, new DateTimeStub(expectTime));
            string expectEmail = "expect@email.com";
            string expectComment = "expect";
            bool expectSendNews = true;

            CreateContractCommand.AddressType expectAddress = new CreateContractCommand.AddressType()
            {
                City = "City",
                Country = "Country"
            };

            // Act
            var result = sut.Handle(new CreateContractCommand
            {
                Email = expectEmail,
                Comment = expectComment,
                Accept = true,
                Address = new CreateContractCommand.AddressType()
                {
                    City = expectAddress.City,
                    Country = expectAddress.Country
                },
                Gender = 0, //gender male female
                SendNews = expectSendNews,
                Tags = new [] {"Tag1, Tag2"}

            }, CancellationToken.None);

            // Assert
            _mediatorMock.Verify(m => m.Publish(It.Is<ContractCreated>(cc =>
                cc.Email == expectEmail &&
                cc.Id != Guid.Empty), It.IsAny<CancellationToken>()), Times.Once);

            var actualContact = _contact;

            Assert.AreNotEqual(Guid.Empty, actualContact.Id);
            Assert.AreEqual(expectEmail, actualContact.Email);
            Assert.AreEqual(expectComment, actualContact.Comment);
            Assert.AreEqual(expectAddress.City, actualContact.City);
            Assert.AreEqual(expectAddress.Country, actualContact.Country);
            Assert.AreEqual(expectSendNews, actualContact.SendNews);
            Assert.AreEqual(expectTime, actualContact.CreatedDate);
        }
    }
}

