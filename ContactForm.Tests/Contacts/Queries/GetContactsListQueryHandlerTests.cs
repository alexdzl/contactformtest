﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using ContactForm.App.Contacts.Queries.GetContactsList;
using ContactForm.App.Infrastructure.AutoMapper;
using ContactForm.App.Interfaces;
using ContactForm.Domain;
using Moq;
using NUnit.Framework;

namespace ContactForm.Tests.Contacts.Queries
{
    class GetContactsListQueryHandlerTests
    {
        protected Mock<IUnitOfWork> _unitOfWork;
        protected IMapper _mapper;

        [SetUp]
        public void Setup()
        {
            var mappingConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new AutoMapperProfile());
            });
            _unitOfWork = new Mock<IUnitOfWork>();

            Mock<IAsyncRepository<Contact>> repositoryMock = new Mock<IAsyncRepository<Contact>>();
            _unitOfWork.Setup(uow => uow.ContactRepository).Returns(repositoryMock.Object);
            repositoryMock.Setup(r => r.Get()).ReturnsAsync(() =>
            {
                var list = new List<Contact>();
                list.Add(new Contact("11","11", new[] { "tag1" }, "city1", "country1", true));
                list.Add(new Contact("22", "22", new[] { "tag1" }, "city2", "country2", true));
                return list;
            });


            _mapper = mappingConfig.CreateMapper();
        }

        [Test]
        public async Task Handle_QueryRequest_ShouldReturnList()
        {
            // Arrange
            var sut = new GetContactsListQueryHandler(_unitOfWork.Object, _mapper);

            // Act
            var result = await sut.Handle(new GetContactsListQuery() ,CancellationToken.None);

            // Assert
            Assert.IsInstanceOf<ContactsListViewModel>(result);
            Assert.That(result.Contacts, Has.Count.EqualTo(2));
        }
    }

}
