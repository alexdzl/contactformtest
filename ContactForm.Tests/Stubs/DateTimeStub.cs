﻿using System;
using ContactForm.Common;

namespace ContactForm.Tests.Stubs
{
    class DateTimeStub : IDateTime
    {
        public DateTimeStub(DateTime date)
        {
            Now = date;
            UtcNow = date;
        }
        public DateTime Now { get; }
        public DateTime UtcNow { get; }
    }
}
