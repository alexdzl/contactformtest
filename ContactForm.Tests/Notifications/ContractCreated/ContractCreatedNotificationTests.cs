using System;
using System.Threading;
using ContactForm.App.Interfaces;
using ContactForm.App.Notifications;
using Moq;
using NUnit.Framework;

namespace ContactForm.Tests.Notifications.ContractCreated
{
    public class ContractCreatedNotificationTests
    {

        [Test]
        public void Handle_GivenNotification_ShouldSendMessage()
        {
            // Arrange
            Mock<INotificationService> notificationService = new Mock<INotificationService>();
            var sut = new App.Contacts.CreateContactCommand.ContractCreated.ContactCreatedHandler(notificationService.Object);
            var expectSubject = "ContractCreated";
            var expectId = Guid.NewGuid();

            // Act
            var result = sut.Handle(new App.Contacts.CreateContactCommand.ContractCreated()
            {
                Id = expectId
            }, CancellationToken.None);


            // Assert
            notificationService.Verify(m => m.SendAsync(It.Is<Message>(cc =>
                cc.Subject == expectSubject
                && cc.Body == expectId.ToString()
            )), Times.Once);
        }
    }
}