using System;

namespace ContactForm.Domain
{
    public class Contact: BaseEntity
    {
        public Contact()
        {
        }

        public Contact(string email, string comment, string[] tags, string city, string country, bool sendNews, Gender? gender = null)
        {
            Email = email;
            Comment = comment;
            Tags = tags;
            Id = Guid.NewGuid();
            City = city;
            Country = country;
            SendNews = sendNews;
        }

        public string Email { get;  set; }
        public string Comment { get;  set; }
        public string City { get;  set; }
        public string Country { get;  set; }
        public string[] Tags { get;  set; }
        public Gender? Gender { get;  set; }
        public bool SendNews { get;  set; }
    }

    public enum Gender
    {
        Male = 1,
        Female = 2
    }

}