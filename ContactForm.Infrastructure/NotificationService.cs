﻿using System.Threading.Tasks;
using ContactForm.App.Interfaces;
using ContactForm.App.Notifications;

namespace ContactForm.Infrastructure
{
    class NotificationService : INotificationService
    {
        public Task SendAsync(Message message)
        {
            return Task.CompletedTask;
        }
    }
}
